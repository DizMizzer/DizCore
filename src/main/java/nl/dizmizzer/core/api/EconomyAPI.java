package nl.dizmizzer.core.api;

import nl.dizmizzer.core.data.SQLManager;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class EconomyAPI {

    private static EconomyAPI instance = new EconomyAPI();
    public static EconomyAPI get() {
        return instance;
    }

    /**
     *
     * @param UUID UUID of the player.
     * @return Amount of money from player with UUID
     */
    public int getMoney(String UUID) {
        return SQLManager.get().getEconomy(UUID);
    }

    /**
     * Use this function to change the amount of money of a player.
     *
     * @param UUID The UUID of the player you want to change.
     * @param amount Amount of money added or removed from the player.
     */
    public void changeMoney(String UUID, int amount) {
        SQLManager.get().addEconomy(UUID, amount);
    }

}
