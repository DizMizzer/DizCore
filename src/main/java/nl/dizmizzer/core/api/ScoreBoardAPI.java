package nl.dizmizzer.core.api;

import nl.dizmizzer.core.DizUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class ScoreBoardAPI {

    private HashMap<Integer, String> lines;
    private String title = "";
    private int linecount = 16;

    /**
     * Start a new Scoreboard.
     */
    public ScoreBoardAPI() {
        this.lines = new HashMap<>();
        for (int i = 0; i < 16; i++) {
            this.lines.put(i, emptyToString(i));
        }
    }

    /**
     * Use this line to easily add a player name.
     *
     * @param line       Which line you want to use.
     * @param text       The line of text. This has to include %player%
     * @param playername The name of the player you want to target.
     * @return The updated Scoreboard.
     */
    public ScoreBoardAPI addPlayerLine(int line, String text, String playername) {
        if (!text.contains("%player%")) return this;

        this.lines.put(line - 1, DizUtils.toColor(text.replace("%player%", playername)));
        return this;
    }

    /**
     * @param line  Which line you want to use.
     * @param text  The line of text. This has to include %money%
     * @param money The amount of money you want to show.
     * @return The updated Scoreboard.
     */

    public ScoreBoardAPI addMoneyLine(int line, String text, int money) {
        if (!text.contains("%money%")) return this;

        this.lines.put(line - 1, DizUtils.toColor(text.replace("%money%", money + "")));

        return this;
    }

    /**
     * @param line Which line you want to use.
     * @param text The line of text.
     * @return The updated Scoreboard.
     */
    public ScoreBoardAPI addLine(int line, String text) {
        this.lines.put(line - 1, DizUtils.toColor(text));
        return this;
    }

    /**
     * @param s The title of your Scoreboard.
     * @return The updated Scoreboard.
     */
    public ScoreBoardAPI setTitle(String s) {
        this.title = DizUtils.toColor(s);
        return this;
    }

    /**
     * @param i the amount of lines you want to use. Has to be 16 or lower.
     * @return The updated Scoreboard.
     */
    public ScoreBoardAPI setLines(int i) {
        if (i > 16) return this;
        this.linecount = i;
        return this;
    }

    /**
     * @return The scoreboard which you can show to players.
     */
    public Scoreboard buildScoreboard() {
        Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();

        Objective obj = sb.registerNewObjective("test", "dummy");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(title);
        for (int i = 1; i < linecount + 1; i++) {
            Score s = obj.getScore(lines.get(i));
            s.setScore(i);
        }
        return sb;
    }


    public String emptyToString(int amount) {
        String string = "";
        for (int i = 0; i < amount; i++) {
            string = string + ChatColor.RESET;
        }
        return string;
    }

}
