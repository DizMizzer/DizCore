package nl.dizmizzer.core.data;

import nl.dizmizzer.core.DizUtils;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class StringManager {

    static StringManager instance = new StringManager();

    public static StringManager get() {
        return instance;
    }
    public String getString(String ask) {
        if (strings.contains(ask)) return DizUtils.toColor(strings.getString(ask));
        return "";
    }

    private FileConfiguration strings;

    private File file;
    public void setup(Plugin p) {

        if (!p.getDataFolder().exists()) {
            p.getDataFolder().mkdir();
        }

        file = new File(p.getDataFolder(), "strings.yml");
        try {
            if (!file.createNewFile()) {
                strings = YamlConfiguration.loadConfiguration(file);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        strings = YamlConfiguration.loadConfiguration(file);

    }

    public void reload() {
        strings = YamlConfiguration.loadConfiguration(file);
    }
}
