package nl.dizmizzer.core.data;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.sql.*;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class SQLManager {

    static SQLManager sql = new SQLManager();

    private Connection con;

    public static SQLManager get() {
        return sql;
    }

    public synchronized void connect() {
        con = null;

        String url = "jdbc:mysql://localhost:3306/dizcore";
        String user = "root";
        String password = "password"; //Not the actual password

        try {
            con = DriverManager.getConnection(url, user, password);
            Bukkit.getLogger().info("Connected!");
            PreparedStatement pst = con.prepareStatement("CREATE TABLE IF NOT EXISTS `Players` (ID int PRIMARY KEY auto_increment, " +
                    "Name varchar(255), " +
                    "UUID varchar(255), " +
                    "Money int)");

            pst.executeUpdate();
            Bukkit.getLogger().info("Database loaded successfully!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void closeConnection() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPlayer(Player player) {
        String UUID = player.getUniqueId().toString();
        String name = player.getName();
        try {
            PreparedStatement pst = con.prepareStatement("SELECT UUID FROM `Players` WHERE UUID = '" + UUID + "'");
            ResultSet rs = pst.executeQuery();

            if (!rs.absolute(1)) {
                pst = con.prepareStatement("INSERT INTO `Players` (Name, UUID, Money) VALUES ('" + name + "', '" + UUID + "', 0)");
                pst.executeUpdate();
            } else {
                pst = con.prepareStatement("UPDATE `Players` SET Name = '" + name + "' WHERE UUID = '" + UUID + "'");
                pst.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    public int getEconomy(String uuid) {
        PreparedStatement pst = null;
        try {
            pst = con.prepareStatement("SELECT Money FROM `Players` WHERE UUID = '" + uuid + "'");
            ResultSet rs = pst.executeQuery();
            if (rs.next())
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void addEconomy(String uuid, int money) {
        PreparedStatement pst = null;
        try {
            pst = con.prepareStatement("UPDATE `Players` SET Money = Money + " + money + " WHERE UUID = '" + uuid + "'");
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
