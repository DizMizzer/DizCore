package nl.dizmizzer.core;

import ca.CommandAPI.CommandListener;
import ca.CommandAPI.CommandManager;
import nl.dizmizzer.core.data.SQLManager;
import nl.dizmizzer.core.data.StringManager;
import nl.dizmizzer.core.events.PlayerListener;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class Core extends JavaPlugin {

    public void onEnable() {
        register(new PlayerListener(this));
        register(new ReloadString());
        SQLManager.get().connect();
        StringManager.get().setup(this);
    }

    private void register(Listener... l) {
        for (Listener li : l) {
            getServer().getPluginManager().registerEvents(li, this);
        }
    }

    private void register(CommandListener... c) {
        CommandManager.register(c);
    }

    public void onDisable() {
        SQLManager.get().closeConnection();
    }
}
