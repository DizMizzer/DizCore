package nl.dizmizzer.core;

import ca.CommandAPI.Command;
import ca.CommandAPI.CommandClass;
import ca.CommandAPI.CommandListener;
import nl.dizmizzer.core.data.StringManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class ReloadString implements CommandListener {

    @Command(permission = "core.reloadstring")
    public void reloadString(CommandSender player, String[] args) {
        StringManager.get().reload();
    }
}
