package nl.dizmizzer.core.events;

import nl.dizmizzer.core.Core;
import nl.dizmizzer.core.data.SQLManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class PlayerListener implements Listener {

    Core core;
    public PlayerListener(Core core) {
        this.core = core;
    }
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        SQLManager.get().addPlayer(e.getPlayer());
    }

}
